package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HeaderComponent {

    @FindBy(id = "twotabsearchtextbox")
    private WebElement searchField;

    @FindBy(css = "form.nav-searchbar input[type='submit']")
    private WebElement searchBtn;

    public HeaderComponent(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public void search(String value) {
        searchField.sendKeys(value);
        searchBtn.click();
    }
}
